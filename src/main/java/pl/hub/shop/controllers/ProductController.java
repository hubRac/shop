package pl.hub.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.hub.shop.models.Product;
import pl.hub.shop.services.ShopService;

import java.util.List;

@RestController
public class ProductController {

    private final ShopService shopService;

    @Autowired
    public ProductController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping
    public List<Product> getProducts() {
        return shopService.getProducts();
    }

    public boolean addProduct(Product product) {
        return shopService.addProduct(product);
    }

}
