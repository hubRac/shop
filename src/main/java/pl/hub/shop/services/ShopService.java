package pl.hub.shop.services;

import org.springframework.stereotype.Service;
import pl.hub.shop.models.Product;

import java.util.List;

@Service
public interface ShopService {
    boolean addProduct(Product product);
    List<Product> getProducts();
}
