package pl.hub.shop.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.hub.shop.config.annotations.ProMode;
import pl.hub.shop.models.Product;
import pl.hub.shop.repositories.FakeShopRepository;
import pl.hub.shop.utils.VatRate;
import pl.hub.shop.utils.VatUtils;

import java.util.Currency;

@Service
@ProMode
public class ShopServiceProImpl extends AbstractShop implements ShopService {

    @Value("${shoppro.vatRate}")
    private VatRate vatRate;
    @Value("${shoppro.discount}")
    private int discount;

    public ShopServiceProImpl(FakeShopRepository fakeShopRepository) {
        super(fakeShopRepository);
    }

    @Override
    public boolean addProduct(Product product) {
        if (product != null && product.getPrice() > 0) {
            double priceAfterVatInclude = VatUtils.calculatePriceWithVat(product.getPrice(), vatRate);
            double priceAfterDisc = calculatePriceAfterDiscount(priceAfterVatInclude);
            System.out.println("Uwzględniono zniżkę w wysokości " + discount + "% oraz podatek VAT wynoszący " + vatRate.getRateAsPercent() + "%");
            product.setPrice(Double.parseDouble(String.format("%.2f",priceAfterDisc).replace(",",".")));
            fakeShopRepository.add(product);
            System.out.println("Dodano produkt " + product.toString());
            return true;
        }
        System.out.println("Produkt jest błedny...");
        return false;
    }

    private double calculatePriceAfterDiscount(double price) {
        return price - price * discount / 100.0;
    }
}
