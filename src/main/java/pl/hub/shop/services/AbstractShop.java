package pl.hub.shop.services;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.hub.shop.models.Product;
import pl.hub.shop.repositories.FakeShopRepository;

import java.security.SecureRandom;
import java.util.List;

@Service
public abstract class AbstractShop implements ShopService {

    protected final FakeShopRepository fakeShopRepository;

    public AbstractShop(FakeShopRepository fakeShopRepository) {
        this.fakeShopRepository = fakeShopRepository;
    }

    @Override
    public List<Product> getProducts() {
        return fakeShopRepository.getProducts();
    }


    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < 5; i++) {
            addProduct(new Product("nowy produkt nr " + i, secureRandom.nextInt(251) + 50));
        }

        fakeShopRepository.getProducts().forEach(System.out::println);

    }
}
