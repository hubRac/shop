package pl.hub.shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.hub.shop.config.annotations.StartMode;
import pl.hub.shop.models.Product;
import pl.hub.shop.repositories.FakeShopRepository;

@Service
@StartMode
public class ShopServiceStartImpl extends AbstractShop implements ShopService {

    @Autowired
    public ShopServiceStartImpl(FakeShopRepository fakeShopRepository) {
        super(fakeShopRepository);
    }

    @Override
    public boolean addProduct(Product product) {
        if (product != null && product.getPrice() > 0) {
            fakeShopRepository.add(product);
            System.out.println("Dodano produkt: " + product.toString());
            return true;
        }
        System.out.println("Produkt jest błedny...");
        return false;
    }

}
