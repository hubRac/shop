package pl.hub.shop.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.hub.shop.config.annotations.PlusMode;
import pl.hub.shop.models.Product;
import pl.hub.shop.repositories.FakeShopRepository;
import pl.hub.shop.utils.VatRate;
import pl.hub.shop.utils.VatUtils;

@Service
@PlusMode
public class ShopServicePlusImpl extends AbstractShop implements ShopService {

    @Value("${shopplus.vatRate}")
    private VatRate vatRate;

    public ShopServicePlusImpl(FakeShopRepository productRepository) {
        super(productRepository);
    }

    @Override
    public boolean addProduct(Product product) {
        if (product != null && product.getPrice() > 0) {
            System.out.println("Doliczono podatek vat w wysokości " + vatRate.getRateAsPercent() + "%");
            double newProductPriceAfterVatInclude = VatUtils.calculatePriceWithVat(product.getPrice(), vatRate);
            product.setPrice(Double.parseDouble(String.format("%.2f",newProductPriceAfterVatInclude).replace(",",".")));
            fakeShopRepository.add(product);
            System.out.println("Dodano produkt: "+product.getPrice());
            return true;
        }
        System.out.println("Produkt jest błedny...");
        return false;
    }
}
