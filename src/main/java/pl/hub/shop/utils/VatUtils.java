package pl.hub.shop.utils;

public class VatUtils {
    public static double calculatePriceWithVat(double basePrice, VatRate vatRate) {
        return 100 * basePrice / (100 - vatRate.getRateAsPercent());
    }
}
