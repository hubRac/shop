package pl.hub.shop.utils;

public enum VatRate {
    STANDARD(23),
    LOWER(8);

    private final int rate;

    VatRate(int rate) {
        this.rate = rate;
    }

    public int getRateAsPercent() {
        return rate;
    }

}
