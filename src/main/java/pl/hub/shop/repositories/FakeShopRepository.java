package pl.hub.shop.repositories;

import org.springframework.stereotype.Repository;
import pl.hub.shop.models.Product;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FakeShopRepository {
    private final List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }

    public void add(Product product) {
        products.add(product);
    }
}
